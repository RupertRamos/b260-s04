<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S04: Access Modifiers and Encapsulation</title>
</head>
<body>

	<h1>Access Modifiers</h1>

	<h3>Building Variables</h3>

	
	<!-- <p><?//php echo $building->floors; ?></p> -->
	<p><?php echo $building->address; ?></p>

	<h4>Mini-Activity</h4>
	<p>The building has <?php echo $building->getFloors(); ?> floors.</p>
	<?php $condominium->setFloors(12); ?>
	<p>The condominium now has <?php echo $condominium->getFloors(); ?> floors.</p>

	



	<h3>Condominium Variables</h3>

	<!-- <p><?//php echo $condominium->name; ?></p> -->
	<!-- <p><?//php echo $condominium->floors; ?></p> -->
	<p><?php echo $condominium->address; ?></p>

	<h1>Encapsulation</h1>

	<p>The name of the condominium is <?php echo $condominium->getName(); ?></p>

	<?php $condominium->setName('Enzo Tower'); ?>

	<p>The name of the condominium has been changed to <?php echo $condominium->getName(); ?></p>




</body>
</html>